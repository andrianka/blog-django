from . import views
from django.urls import path
from .views import BlogMonthArchiveView

urlpatterns = [
    path("", views.home, name="home"),
    path("about_us/", views.about_us, name="about"),
    path("archive_blog/", views.archive_blog, name="archive"),
    path('archive_blog/<int:year>/<int:month>/',
         BlogMonthArchiveView.as_view(
            month_format='%m',
         ),
         name="blog_archive_month",
         ),
    path("contact/", views.contact, name="contact"),
    path('success/', views.success, name='success'),
]
