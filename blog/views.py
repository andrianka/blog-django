from django.shortcuts import render, redirect
from django.db.models import Count
from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse
from django.views.generic.dates import MonthArchiveView
from post.models import Post, Tag, Category
from services.paginator_helper import PaginatorHelper
from services.post_helper import PostHelper
from .forms import ContactForm


def home(request):
    blog_posts_list = Post.objects.all()
    posts = blog_posts_list.reverse()[:10]
    featured_posts = Post.objects.get_featured()
    latest_posts = Post.objects.latest_posts(5)
    tags = Tag.objects.all()

    page_request = request.GET.get("page", 1)
    queryset = PaginatorHelper.get_result(blog_posts_list, page_request, 5)

    context = {
        "posts": posts,
        "featured_posts": featured_posts,
        "blog_posts": queryset,
        "latest_posts": latest_posts,
        "tags": tags,
    }
    return render(request, "home.html", context)


def contact(request):
    if request.method == 'GET':
        form = ContactForm()
    else:
        form = ContactForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            from_email = form.cleaned_data['from_email']
            message = form.cleaned_data['message']
            subject = name
            try:
                send_mail(subject, message, from_email, ['admin@example.com'])
            except BadHeaderError:
                return HttpResponse('Invalid header found.')
            return redirect('success')
    return render(request, "contact.html", {'form': form})


def success(request):
    return HttpResponse('Success! Thank you for your message.')


def about_us(request):
    return render(request, "about_us.html", {})


def archive_blog(request):
    queryset = (
        PostHelper(request).search()
        if request.GET.get("q")
        else Post.archived_objects.all()
    )
    featured = queryset.get_featured().last()
    latest = queryset.latest_posts(5)
    categories = Category.objects.filter(post__archived=True).annotate(
        num_posts=Count("post")
    )
    tags = Tag.objects.filter(post__archived=True).annotate(
        num_posts=Count("post")
    )
    context = {
        "queryset": queryset,
        "featured_post": featured,
        "latest_posts": latest,
        "categories": categories,
        "tags": tags,
    }
    return render(request, "archive_blog.html", context)


class BlogMonthArchiveView(MonthArchiveView):
    queryset = Post.archived_objects.all()
    date_field = 'created_at'
    allow_empty = True
    template_name = 'archive_blog.html'
    make_object_list = True
    featured = queryset.get_featured().last()
    latest = queryset.latest_posts(5)
    blog_categories = Category.objects.filter(post__archived=True).annotate(
        num_posts=Count("post")
    )
    tags = Tag.objects.filter(post__archived=True).annotate(
        num_posts=Count("post")
    )

    def get_context_data(self, **kwargs):
        context = super(BlogMonthArchiveView, self).get_context_data(**kwargs)
        context = {
            "queryset": context['object_list'],
            "featured_post": BlogMonthArchiveView.featured,
            "latest_posts": BlogMonthArchiveView.latest,
            "blog_categories": BlogMonthArchiveView.blog_categories,
            "tags": BlogMonthArchiveView.tags,
        }

        return context
