from django import forms


class ContactForm(forms.Form):
    from_email = forms.EmailField(widget=forms.TextInput(attrs={
        'type': 'email',
        'id': 'contact-email',
        'class': 'form-control',
        'placeholder': 'Email',
    }), required=True, label='')

    name = forms.CharField(widget=forms.TextInput(attrs={
        'id': 'contact-name',
        'placeholder': 'Name',
        'class': 'form-control',
    }), required=True, label='')

    message = forms.CharField(widget=forms.Textarea(attrs={
        'id': 'message',
        'placeholder': 'Message',
        'cols': '30',
        'rows': '10',
        'class': 'form-control'
    }), required=True, label='')
