from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


class PaginatorHelper:
    def get_result(queryset, page, per_page):
        paginator = Paginator(queryset, per_page)
        try:
            queryset = paginator.page(page)
        except PageNotAnInteger:
            queryset = paginator.page(1)
        except EmptyPage:
            queryset = paginator.page(paginator.num_pages)
        return queryset
