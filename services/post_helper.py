from django.db.models import Q
from post.models import Post


class PostHelper:
    def __init__(self, request):
        self.request = request

    def is_archive(self):
        query_type = self.request.GET.get('t')
        if query_type:
            return 'archive' in query_type
        else:
            return False

    def get_posts(self):
        if self.is_archive():
            queryset = Post.archived_objects.all()
        else:
            queryset = Post.objects.all()
        return queryset

    def filter_by_tag(self):
        queryset = self.get_posts()
        query_tag = self.request.GET.get('q')
        if query_tag:
            queryset = queryset.filter(tags__name__contains=query_tag)
        return queryset

    def filter_by_category(self, category_id):
        queryset = self.get_posts()
        return queryset.posts_by_category(category_id)

    def search(self):
        queryset = self.get_posts()
        query = self.request.GET.get('q')
        if query:
            queryset = queryset.filter(
                Q(title__icontains=query) | Q(content__icontains=query)
            ).distinct()
        return queryset
