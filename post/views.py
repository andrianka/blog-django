from django.db.models import Count
from django.shortcuts import render, get_object_or_404
from .models import Post, Tag, Category
from .forms import CommentForm
from services.paginator_helper import PaginatorHelper
from services.post_helper import PostHelper


def index(request):
    return render(request, "index.html", {})


def by_tag(request):
    queryset = PostHelper(request).filter_by_tag()
    page = request.GET.get("page", 1)
    queryset = PaginatorHelper.get_result(queryset, page, 5)
    context = {"queryset": queryset}
    return render(request, "index.html", context)


def by_category(request, category_id):
    queryset = PostHelper(request).filter_by_category(category_id)
    page = request.GET.get('page', 1)
    queryset = PaginatorHelper.get_result(queryset, page, 5)
    context = {'queryset': queryset}
    return render(request, 'index.html', context)


def search(request):
    queryset = PostHelper(request).search()
    context = {'queryset': queryset}
    return render(request, 'index.html', context)


def search_archive(request):
    queryset = PostHelper(request).search()
    context = {'queryset': queryset}
    return render(request, 'archive_blog.html', context)


def detail(request, id):
    post = get_object_or_404(Post, pk=id)
    queryset = Post.archived_objects.all()
    categories = Category.objects.filter(post__archived=False).annotate(
        num_posts=Count("post")
    )
    tags = Tag.objects.filter(post__archived=False).annotate(
        num_posts=Count("post")
    )
    post_tags = post.tags.all()
    latest_posts = Post.objects.latest_posts(5)
    related_posts = Post.objects.filter(tags__in=tags)[:2]
    comments = post.comments.filter(approved=True)
    new_comment = None

    if request.method == 'POST':
        comment_form = CommentForm(data=request.POST)
        if comment_form.is_valid():
            new_comment = comment_form.save(commit=False)
            new_comment.post = post
            new_comment.reply_id = request.GET.get('comment_id')
            new_comment.save()
    else:
        comment_form = CommentForm()

    context = {
        'queryset': queryset,
        'post': post,
        'post_tags': post_tags,
        'tags': tags,
        'latest_posts': latest_posts,
        'categories': categories,
        'related_posts': related_posts,
        'comments': comments,
        'new_comment': new_comment,
        'comment_form': comment_form,
    }
    return render(request, 'single_post.html', context)


def create_comment(request):

    pass


def update_post(reques, id):
    pass
