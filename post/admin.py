from django.contrib import admin

from .models import Post, Category, Author, Tag, Comment


class PostAdmin(admin.ModelAdmin):
    model = Post

    def get_queryset(self, request):
        return self.model.objects_unfiltered.all()


class CommentAdmin(admin.ModelAdmin):
    list_display = ('name', 'content', 'post', 'created_at', 'approved')
    list_filter = ('approved', 'created_at')
    search_fields = ('name', 'email', 'content')
    actions = ['approve_comments']

    def approve_comments(self, request, queryset):
        queryset.update(approved=True)


admin.site.register(Post, PostAdmin)
admin.site.register(Comment)
admin.site.register(Category)
admin.site.register(Tag)
admin.site.register(Author)
