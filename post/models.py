from django.db import models
from django.contrib.auth import get_user_model
from django.urls import reverse


User = get_user_model()


class Author(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    picture = models.ImageField()

    def __str__(self):
        return f"{self.first_name } {self.last_name}"


class Category(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "categories"


class Tag(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class PostQuerySet(models.query.QuerySet):
    def get_featured(self):
        return self.filter(featured=True)

    def latest_posts(self, quantity=3):
        return self.order_by("-created_at")

    def posts_by_category(self, category_id):
        return self.filter(category_id=category_id)


class PostArchiveManager(models.Manager):
    def get_queryset(self):
        return super(PostArchiveManager, self).get_queryset().filter(archived=True)


class PostManager(models.Manager):
    def get_queryset(self):
        return super(PostManager, self).get_queryset().filter(archived=False)


class Post(models.Model):
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    category = models.ForeignKey(
        Category,
        null=True,
        on_delete=models.SET_NULL
        )
    title = models.CharField(max_length=120)
    content = models.TextField()
    picture = models.ImageField()
    featured = models.BooleanField(default=False)
    archived = models.BooleanField(default=False)
    tags = models.ManyToManyField(Tag)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = PostManager.from_queryset(PostQuerySet)()
    archived_objects = PostArchiveManager.from_queryset(PostQuerySet)()
    objects_unfiltered = models.Manager()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("post_detail", kwargs={"id": self.id})


class Comment(models.Model):
    post = models.ForeignKey(
        Post,
        on_delete=models.CASCADE,
        related_name='comments'
        )
    reply = models.ForeignKey(
        'Comment',
        on_delete=models.CASCADE,
        null=True,
        related_name='replies'
        )
    name = models.CharField(max_length=50)
    email = models.EmailField()
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    approved = models.BooleanField(default=False)

    class Meta:
        ordering = ['created_at']

    def __str__(self):
        return self.content
