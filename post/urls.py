from django.urls import path
from . import views


urlpatterns = [
    path("posts/", views.index, name="posts"),
    path("posts/<int:id>/", views.detail, name="post_detail"),
    path(
        "category/<int:category_id>/post/<int:id>",
        views.detail,
        name="post_detail_by_category",
    ),
    path(
        "category/<int:category_id>/posts",
        views.by_category,
        name="posts_by_category"
    ),
    path("search", views.search, name="search"),
    path("search_archive", views.search_archive, name="search_archive"),
    path("tag", views.by_tag, name="posts_by_tag"),
    path("archive_blog/tag", views.by_tag, name="archive_posts_by_tag"),
]
