from .models import Comment
from django import forms


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('name', 'email', 'content')

    email = forms.EmailField(widget=forms.TextInput(attrs={
        'type': 'email',
        'id': 'contact-email',
        'class': 'form-control',
        'placeholder': 'Email',
    }), required=True, label='')

    name = forms.CharField(widget=forms.TextInput(attrs={
        'id': 'contact-name',
        'placeholder': 'Name',
        'class': 'form-control',
    }), required=True, label='')

    content = forms.CharField(widget=forms.Textarea(attrs={
        'id': 'message',
        'placeholder': 'Comment',
        'cols': '30',
        'rows': '10',
        'class': 'form-control'
    }), required=True, label='')
